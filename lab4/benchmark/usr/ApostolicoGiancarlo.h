//
// Created by bokoch on 12/4/17.
//

#ifndef APOSTOLICOGIANCARLO_H
#define APOSTOLICOGIANCARLO_H


#include <vector>
#include <cstdio>
#include <iostream>
#include <vector>
#include <algorithm>
#include <math.h>
#include <unordered_map>
#include <deque>
#include <cstdlib>
typedef struct _Tposition{
    int line;
    int row;
    int skip;
} TPosition;

//Длина наибольшего суффикса, который является суффиксом всей строки
void BuildNblock(const std::vector<std::pair<std::string, int > > &,
                 std::vector<int>&, const int&);
void PreBmBc(std::vector<std::pair<std::string, int> > &,
             std::vector<int>&, const int&);
void BuildLblock(std::vector<int>&, const std::vector<int>&, const int&);
void BuildlBlock(std::vector<int>&, const std::vector<int>&, const int&);

int GetGoodSuffRule(std::vector<int>&, const std::vector<int>&,
                     const int&, const int&);

int GetBadCharactherRule(std::unordered_map<std::string, int>&,
                          std::deque<std::pair <std::string, TPosition> > &,
                          std::vector<int>&,
                          const int&, const int&);

#endif //LAB4_APOSTOLICOGIANCARLO_H
