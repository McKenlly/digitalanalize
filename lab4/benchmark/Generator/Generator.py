# -*- coding: utf-8 -*-
import sys
import random
import string
import copy
from random import choice
zero = 10

if __name__ == "__main__":
    wordsInPattern = 0
    wordsInText = 0
    setNum = 0
    count_of_tests = 0
    if len (sys.argv) == 5:
        wordsInPattern = int(sys.argv[1])
        wordsInText = 200
        count_of_tests = int(sys.argv[3])
        setNum = int(sys.argv[4])
    else:
        print("Error: add arguments:\n")
        print("\t *Number Word in Pattern\n")
        print("\t *Number Word in Text\n")
        print("\t *Number test\n")
        print("\t *Range digits\n")
        sys.exit(1)
    fileTest = open('buildGnuplot/countTest.txt', 'a')
    space = [ "\n", " ", "\t", " ", " ", " ", "\t"]
    print("Genarate tests")
    for enum in range( count_of_tests ):
        fileTest.write("{0}\n".format( wordsInText ))
        test_file_name = "tests/task/{:02d}".format( enum + 1 )
        with open( "{0}.t".format( test_file_name ), 'w' ) as output_file:
            print("Make test:\t", test_file_name, ".t")
            for _ in range( wordsInPattern ):
                num = random.randint(0, setNum)
                countZero = random.randint(0, zero)
                output_file.write("{0}{1} ".format( "0"*countZero, num ))
            output_file.write("\n")
            for _ in range( wordsInText ):
                num = random.randint(0, setNum)
                countZero = random.randint(0, zero)
                spaceOut = random.choice(space)
                output_file.write("{0}{1}{2} ".format( spaceOut, "0"*countZero, num ))
            wordsInText *= 2
                #answer_file.write( "{0}\n".format( answer ) )
