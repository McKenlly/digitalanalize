#!/usr/bin/gnuplot -persist

set title "TIME GRAPH FOR SIZE PATTERN 10"
set xlabel "SIZE TEXT"
set ylabel "TIME"
set terminal jpeg size 640,480

set output "graph.jpg"
set style data linespoints
plot "OverRuntime" u  1:2 title "Aho-Corasic", \
     "OverRuntime" u  1:3 title "Apostolico-Guiancarlo" 
