#include <vector>
#include <iostream>
#include <unordered_map>
#include <cstdio>
#include <boost/regex.hpp>
#include <boost/algorithm/string/find.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/algorithm/string/find_iterator.hpp>
#include <boost/algorithm/string.hpp>

bool IsDigit(const char &c);
void PatternParsing(std::string& pattern);
bool TextParsing(std::unordered_map<unsigned long long, std::pair<int, int> > & pos, std::string& text);




int main (int argc, char *argv[]) {
    std::string xStr;
    std::string substring;
    PatternParsing(substring);
    std::unordered_map<unsigned long long, std::pair<int, int> > position;
    TextParsing(position, xStr);
    boost::regex xSub(substring);
    typedef boost::iterator_range<std::string::const_iterator> string_range;
    std::vector<string_range> matches;

    boost::smatch m_res;
    unsigned long long cur = 0;
    std::string::const_iterator xItStart = xStr.begin();
    std::string::const_iterator xItEnd = xStr.end();
     while (boost::regex_search (xItStart, xItEnd,m_res,xSub)) {
         std::cout << "MLEN " <<  m_res.length() << " " << m_res.position() << std::endl;
         std::cout << m_res.size() << std::endl;
         if (position.find(cur) != position.end()) {
             std::cout << position[cur].first << " " << position[cur].second << std::endl;
         }
         cur += m_res.length();
         for (auto& x:m_res) {
            std::cout << x.length()  << std::endl;
        }
         std::cout << "m[1] " << *m_res[1].second << std::endl;
        std::cout << std::endl;

         xItStart = m_res[1].second;
//        xStr = m_res.suffix().str();

    }

//    boost::regex_match (xStr,m_res,xSub);
//    {
//        std::cout << m_res.length() << " " << m_res.position() << std::endl;
//        std::cout << m_res.size() << std::endl;
//        for (auto& x:m_res) {
//            std::cout << x.length() <<  /*<< x.position() <<*/ std::endl;
//        }
//
//        std::cout << std::endl;
//
//        xStr = m_res.suffix().str();
//
//    }
    // Outp
//    boost::find_all(matches, xStr, substring);
//    for (auto i:matches) {
//        unsigned long long tmp = i.begin() - xStr.begin()-1;
//        auto findPattern = position.find(tmp);
//        std::cout << i.begin() - xStr.begin()-1 <<std::endl;
//        if (findPattern != position.end()) {
//            printf("%d %d\n", findPattern->second.first, findPattern->second.second);
//        }
//    }
    return 0;
}

bool TextParsing(std::unordered_map<unsigned long long, std::pair<int, int> > & pos, std::string& text) {
    char ch;
    bool leadingZero = true;
    int line = 1;
    int row = 1;

    unsigned long long reccur = 0;
    std::string tmpStr = "";
    while(true) {
        while(!IsDigit(ch = getchar())) {
            if(ch == EOF) {
                return false;
            }
            if (ch == '\n') {
                ++line;
                row = 1;
            }
        }
        do {//ввод слова полностью
            if ((ch == '0' && !leadingZero)) {
                tmpStr += ch;
            }
            if (ch > '0' && ch <= '9') {
                tmpStr += ch;
                leadingZero = false;
            }
        } while(IsDigit(ch = getchar()));
        if (tmpStr.size() == 0) {
            tmpStr = "0";
        }
        pos[reccur].first = line;
        pos[reccur].second = row;
        reccur += tmpStr.size();
        text += tmpStr;
        tmpStr = "";
        if (ch == ' ') {
            row++;
        }
        leadingZero = true;
        std::ungetc(ch, stdin);
    }
    return true;

}



void PatternParsing(std::string &pattern) {
    char ch;
    pattern = "";
    int i = 0;
    bool leadingZero = true;
    bool inWord = false;
    std::string tmpStr = "";
    while ((ch = getchar()) != '\n') {

        if (ch == EOF) {//никогда не выполняется
            return;
        }

        if (IsDigit(ch)) {
            if (ch == '0' && !leadingZero) {
                tmpStr += ch;
            }
            if (ch > '0' && ch <= '9') {
                leadingZero = false;
                tmpStr += ch;
            }
            inWord = true;
        } else { //Если встретили пробел
            if (inWord) {
                if (tmpStr.size() == 0) {
                    tmpStr = "0";
                }
                pattern += tmpStr;
                tmpStr = "";
                leadingZero = true;
                inWord = false;
            }
        }
    }
    if (tmpStr.size() == 0 && inWord) {
        tmpStr = "0";
    }
    if (tmpStr.size()) {
        pattern += tmpStr;
    }
}

bool IsDigit(const char &c) {
    return c >= '0' && c <= '9';
}