#include "AC.h"
#include <iostream>
#include <sstream>
#include <fstream>
#include <ctime>
int main(int argc, char *argv[]) {
    if (argc != 2) {
        printf("NO FILE FOR INSERT TIME PROCESSING\n");
        exit(0);
    }
    clock_t begin = clock();
    std::vector< std::pair<int, int> > answer;
    std::vector<std::string> pattern;
    std::vector<unsigned long> textToSearch;
    std::string input, token;
    TFinitaAuto aho;
  
  std::getline(std::cin, input);
  std::stringstream ss(input);

  while (ss >> token) {
    pattern.emplace_back(std::string(token));
  }

  int lineCount = 1;
  int wordCount = 1;

  while (std::getline(std::cin, input)) {
    std::stringstream textSS (input);

    while (textSS >> token) {
      textToSearch.emplace_back(std::stoul(token));
      answer.emplace_back(std::make_pair(lineCount, wordCount));
      wordCount++;
    }

    lineCount++;
    wordCount = 1;
  }
  
  aho.Build(pattern);
  aho.Search(textToSearch, answer, pattern.size());
  clock_t end = clock();
  double time = (double)(end - begin);
  std::ofstream fout(argv[1], std::ofstream::app);
    if (!fout) {
        perror("No find file");
    }
  fout << time/CLOCKS_PER_SEC << std::endl;
  fout.close();
  return 0;
}
