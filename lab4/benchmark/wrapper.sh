#!/usr/bin/bash
rm AllOverTime
echo "Compile Aho-Carasic"
cd Aho
make
cd ..
echo "Compile Apostoloco-Guiancarlo"
cd usr
make
cd ..
echo "Compile generator"
cd Generator
#sh run.sh
cd ..
echo "Execute tests"
for test_file in $(ls Generator/tests/task/*.t); do
    echo "Execute $test_file"
    answer_file="${test_file:21}"
    new="${answer_file%.*}"
    PATH=$(pwd)
    if ! ./usr/main "${PATH}/Generator/tests/time/timeAG.txt" < $test_file > "./Generator/tests/answer/AG/${new}.pl" ; then
        echo "ERROR: no run generator(Apostoloco-Guiancarlo)"
        continue
    fi
    if ! ./Aho/Aho  "${PATH}/Generator/tests/time/timeAC.txt" < $test_file > "./Generator/tests/answer/AC/${new}.pl" ; then
        echo "ERROR: no run Aho-Korasic"
        continue
    fi
done

