#include "ApostolicoGiancarlo.h"
bool IsDigit(const char &c);
TPosition nowWord = {1, 1, -1};
const unsigned char BEGIN_POS = 0;
void PatternParsing(std::unordered_map<std::string, int> &patternDict,
                    std::vector<std::pair<std::string, int> >& allWordPattern);
bool TextParsing(std::deque<std::pair <std::string, TPosition> >&, const int &);

int main() {
    std::unordered_map<std::string, int> patternDict;
    std::vector<std::pair<std::string, int > > pattern;
    std::deque<std::pair <std::string, TPosition> > text;

    PatternParsing(patternDict, pattern);

    int patternSize = pattern.size();

    bool flag = TextParsing(text, patternSize);

    std::vector<int> N(patternSize, 0);
    std::vector<int> array(patternDict.size(), patternSize);
    std::vector<int> lowerGoodSuff(patternSize+1, 0);
    std::vector<int> L(patternSize, 0);

    /* Preprocessing */
    PreBmBc(pattern, array, patternSize);
    BuildNblock(pattern, N, patternSize);
    BuildLblock(L, N, patternSize);
    BuildlBlock(lowerGoodSuff, N, patternSize);
    /*finish*/

    while(flag) {
        bool isFind = false;
        int i;
        int shift;
        for(i = patternSize - 1; i >= 0; --i) {
            if (text[i].second.skip == -1 || (text[i].second.skip == 0 && N[i] == 0)) {
                if (text[i].first == pattern[i].first) {
                    if (i == 0) {
                        printf("%d, %d\n", text[0].second.line,
                               text[0].second.row);
                        text[patternSize - 1].second.skip = patternSize;
                        shift = patternSize - lowerGoodSuff[1];
                        isFind = true;
                        break;
                    }
                    continue;
                } else {
                    text[patternSize - 1].second.skip = patternSize - 1 - i;
                    break;
                }
            }
            if (text[i].second.skip < N[i]) {
                i -= text[i].second.skip;
                continue;

            } else if (text[i].second.skip >= N[i] && N[i] == i) {
                printf("%d, %d\n", text[BEGIN_POS].second.line,
                       text[BEGIN_POS].second.row);
                shift = patternSize - lowerGoodSuff[1];
                text[patternSize - 1].second.skip = patternSize - 1 - i;
                isFind = true;
                break;
            }
            if (text[i].second.skip > N[i] && N[i] < i) {
                text[patternSize - 1].second.skip = patternSize - 1 - i;
                i -= N[i];
                break;
            }
            if (text[i].second.skip == N[i] && 0 < N[i] && N[i] < i) {
                i -= text[i].second.skip;
            }
        }
        if (!isFind) {
            if (i == -1) {
                printf("%d, %d\n", text[0].second.line,
                       text[0].second.row);
                shift = patternSize - lowerGoodSuff[1];
                text[patternSize - 1].second.skip = patternSize - 1;
                isFind = true;
            } else {
                text[patternSize - 1].second.skip = patternSize - 1 - i;

                int badCharacter = GetBadCharactherRule(patternDict, text,
                                                        array, i, patternSize);

                int goodSuff = GetGoodSuffRule(L, lowerGoodSuff, i, patternSize);

                shift = std::max(badCharacter, goodSuff);
            }
        }
        flag = TextParsing(text, shift);
    }
    return 0;
}

bool TextParsing(std::deque<std::pair<std::string, TPosition> >& text,
                 const int& countNext) {
    int sizePattern = countNext;
    if (text.size()) {
        sizePattern = text.size();
        for(int i = countNext; i > BEGIN_POS; --i) {
            text.pop_front();
        }
    }
    text.resize(sizePattern);
    char ch;
    int i = sizePattern - countNext;
    bool leadingZero = true;
    while(i < sizePattern) {
        while(!IsDigit(ch = getchar())) {
            if(ch == EOF) {
                return false;
            }
            if (ch == '\n') {
                ++nowWord.line;
                nowWord.row = 1;
            }
        }
        do {//ввод слова полностью
            if ((ch == '0' && !leadingZero)) {
                text[i].first += ch;
            }
            if (ch > '0' && ch <= '9') {
                text[i].first += ch;
                leadingZero = false;
            }
        } while(IsDigit(ch = getchar()));
        if (text[i].first.size() == 0) {
            text[i].first = "0";
        }
        text[i].second = nowWord;
        if (ch == ' ') {
            nowWord.row++;
        }
        leadingZero = true;
        std::ungetc(ch, stdin);
        ++i;
    }
    return true;

}



void PatternParsing(std::unordered_map<std::string, int> &patternDict,
                    std::vector<std::pair<std::string, int> >& allWordPattern) {
    char ch;
    int i = 0;
    bool leadingZero = true;
    bool inWord = false;
    std::string tmpStr = "";
    while ((ch = getchar()) != '\n') {

        if (ch == EOF) {//никогда не выполняется
            return;
        }

        if (IsDigit(ch)) {
            if (ch == '0' && !leadingZero) {
                tmpStr += ch;
            }
            if (ch > '0' && ch <= '9') {
                leadingZero = false;
                tmpStr += ch;
            }
            inWord = true;
        } else { //Если встретили пробел
            if (inWord) {
                if (tmpStr.size() == 0) {
                    tmpStr = "0";
                }
                if (patternDict.find(tmpStr) == patternDict.end()) {
                    patternDict[tmpStr] = i;
                    i++;
                }
                allWordPattern.push_back(std::pair<std::string, int>
                                                 (tmpStr, patternDict[tmpStr]));
                tmpStr = "";
                leadingZero = true;
                inWord = false;
            }
        }
    }
    if (tmpStr.size() == 0 && inWord) {
        tmpStr = "0";
    }
    if (tmpStr.size()) {
        if (patternDict.find(tmpStr) == patternDict.end()) {
            patternDict[tmpStr] = i;
        }
        allWordPattern.push_back(std::pair<std::string, int>
                                         (tmpStr, patternDict[tmpStr]));
    }
    return;

}

bool IsDigit(const char &c) {
    return c >= '0' && c <= '9';
}
