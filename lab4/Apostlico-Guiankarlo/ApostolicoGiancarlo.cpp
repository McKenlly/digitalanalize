//
// Created by bokoch on 12/4/17.
//

#include "ApostolicoGiancarlo.h"


void PreBmBc(std::vector<std::pair<std::string, int> > &pattern,
             std::vector<int>& array, const int& size) {
    for (int i = 0; i < size - 1; ++i) {
        array[pattern[i].second] = i;
    }
}


void BuildNblock(const std::vector<std::pair<std::string, int > > &pattern,
                 std::vector<int>& suffix, const int& size) {
    int left = size - 1;
    int right = size - 1;
    for (int i  = size - 1; i >= 0; --i) {
        if (i >= left) {
            suffix[i] = std::min((i-left+1), suffix[size-1-right+i]);
        }

        while (i-suffix[i]>=0&&
                pattern[size-1-suffix[i]].first==pattern[i-suffix[i]].first) {
            ++suffix[i];
        }

        if (i - suffix[i] + 1 < left) {
            left = i - suffix[i] + 1;
            right = i;
        }
    }
}

void BuildLblock(std::vector<int>& largeGoodSuff,
                 const std::vector<int>& suffix, const int& size) {
    for (int j = 0; j < size; j++) {
        int k = size - suffix[j];
        largeGoodSuff[k] = j;
    }
}

void BuildlBlock(std::vector<int>& lowerGoodSuff,
                 const std::vector<int>& suffix, const int& size) {
    lowerGoodSuff[lowerGoodSuff.size() - 1] = 0;
    int j = 0;
    for(int i = 1; i < size; i++) {
        if (suffix[i] == i+1) {
            j = i+1;
        }
        lowerGoodSuff[size - 1 - i] = j;
    }
}

int GetGoodSuffRule(std::vector<int>& largeGoodSuff,
                     const std::vector<int>& lowerGoodSuff,
                     const int& pos, const int& patternSize){
    int answer;
    if (pos == patternSize - 1) {
        answer = 1;
    } else {
        if (pos < patternSize && largeGoodSuff[pos + 1] > 0) {
            answer = patternSize - 1 - largeGoodSuff[pos + 1];
        } else {
            answer = patternSize - lowerGoodSuff[pos + 1];
        }
    }
    return answer;
}

int GetBadCharactherRule(std::unordered_map<std::string, int>& patternDict,
                          std::deque<std::pair <std::string, TPosition> > &text,
                          std::vector<int>& array,
                          const int& pos, const int& size) {
    std::unordered_map<std::string, int>::iterator it =
            patternDict.find(text[pos].first);
    int answer;
    if (it != patternDict.end()) {
        answer = it->second;
        answer = std::max(1, pos - array[answer]);
    } else {
        answer = size;
    }
    return answer;
}
