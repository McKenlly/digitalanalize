#!/usr/bin/bash
case "$1" in
    r|run) echo "Run Apostolico Guiancarlo"
           cd Apostlico-Guiankarlo
           make
           cd ..
           echo "Done. Enter \"pattern\", next across \n enter text"
           echo "Answer will number line and number row, where find occurrence"
           ./Apostlico-Guiankarlo/main
        ;;
    b|benchmark) echo "Run compare AC with AG. After open folder benchmark/Generator/buildGnuplot/ and run file graph.sh, open file ./benchmark/Generator/buildGnuplot/*.png and done"
        sh ./benchmark/wrapper.sh
        ;;
    *) echo "O no, enter action: benchmark or run //(b or r)."
        ;;
esac
