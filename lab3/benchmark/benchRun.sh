source=main.cpp
bin=${source%.*}
rm benchTime
cat bencTime
#if ! python3 test_generator.py 10 ; then
#    echo "ERROR: Failed to python generate tests."
#    exit 1
#fi
for test_file in `ls tests/*.t`; do
    answer_file="${test_file%.*}"
    echo "Execute ${test_file}"
    if !  ./main < $test_file > "${answer_file}.pl" ; then
        echo "ERROR"
        continue
    fi
    if !  ./benchmark < $test_file > "${answer_file}.pro" ; then
        echo "ERROR"
        continue
    fi
    if ! diff -u "${answer_file}.txt"  "${answer_file}.pro" > /dev/null ; then
        echo "Failed"
    else
        echo "OK"
    fi 
done  
 
