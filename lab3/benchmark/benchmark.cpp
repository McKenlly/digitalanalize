#include <cstdio>
#include <string.h>
#include <map>
#include <fstream>
#include <iostream>

void LowerString(std::string &);
void Parsing(char &, std::string &, unsigned long long &);


int main() {
    std::ofstream fout("benchTime", std::ios::app);
    std::map<std::string, unsigned long long> map;
    std::string buffer;
    char action;
    unsigned long long value;
    clock_t start = clock();
    while(true) {
        Parsing(action, buffer, value);
        if(action == 'E') {
            break;
        }
        switch(action) {
            case '+':
                if(map.find(buffer) == map.end()) {
                    map[buffer] = value;
                    printf("OK\n");
                } else {
                    printf("Exist\n");
                }
                break;
            case '-':
                if (map.find(buffer) == map.end()) {
                    printf("NoSuchWord\n");
                }
                else {
                    map.erase(buffer);
                    printf("OK\n");
                }
                break;
            case 'F':
                if (map.find(buffer) == map.end()) {
                    printf("NoSuchWord\n");
                }
                else {
                    printf("OK: %llu\n", map[buffer]);
                }
                break;
        }
    }
    clock_t finish = clock();
    double time = (double) (finish - start)/ CLOCKS_PER_SEC;
    fout << time << std::endl;
    fout.close();
    return 0;
}

void LowerString(std::string& buffer) {
    int index = 0;
    while (index < buffer.size()) {
        if (buffer[index] >= 'A' && buffer[index] <= 'Z') {
            buffer[index] += -'A' + 'a';
        }
        index++;
    }
}

void Parsing(char &action, std::string& buffer, unsigned long long &value) {
    char ch;
    size_t i = 0;
    ch = getchar();
    if (ch == EOF) {
        action = 'E';
        return;
    }
    if (ch == '+') {
        action = ch;
        getchar();
        std::cin >> buffer >>value;
        getchar();
        LowerString(buffer);
    } else if (ch == '-') {
        action = ch;
        getchar();
        std::cin >> buffer;
        getchar();
        LowerString(buffer);
    } else if (ch == '!') {
        getchar();
        action = getchar();
        while((ch = getchar()) != ' ') {
            i++;
        }
        scanf("%s", buffer);
        getchar();
    } else {
        action = 'F';
        buffer[i++] = ch;
        while((ch = getchar()) != '\n') {
            buffer[i++] = ch;
        }
        buffer[i] = '\0';
        LowerString(buffer);
    }
}

