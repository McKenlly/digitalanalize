//
// Created by bokoch on 12.10.17.
//

#include "BTree.h"
TBtree::TBtree(size_t size) {
    if (size < DEFAULT_DEGREE) {
        root = new TNode(DEFAULT_DEGREE);
    } else {
        TNode::DEGREE = size;
        root = new TNode(size);
    }
}

void TBtree::Insert(TData& tmp) {
    root->Insert(&root, tmp);
}

TData *TBtree::SearchWord(char *tmp) {
    if(root) {
        return root->Search(tmp);
    } else {
        return NULL;
    }
}

void TBtree::DeleteNode(char *tmp) {
    root->Delete(tmp);
    if (root->countKeys == 0) {
        TNode *temp = root;
        if (!root->leaf) {
            root = root->cptrs[0];
            temp->cptrs[0] = NULL;
            delete temp;
        }
    }
}

int TBtree::SaveTree(const char* file) {
    FILE* f = fopen(file, "wb");
    if (f == NULL) {
        return -1;
    }
    bool ans = root->SaveInFileNode(f, root);
    fclose(f);
    return ans;
}

int TBtree::LoadTree(const char* file) {
    FILE* f = fopen(file, "rb");
    if (f == NULL) {
        return -1;
    }
    TNode* rootNew = new TNode(TNode::DEGREE);
    bool ans = root->LoadInFileNode(f, rootNew);
    fclose(f);
    if (ans) {
        root->DeleteTree();
        root = rootNew;
        return true;
    }
    else {
        rootNew->DeleteTree();
        return false;
    }
}

TBtree::~TBtree() { 
    root->DeleteTree();
}
/*void TBtree::Print() {
    root->Print();
}*/
