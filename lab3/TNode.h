//
// Created by bokoch on 12.10.17.
//

#ifndef LAB_2_TNODE_H
#define LAB_2_TNODE_H
#include "cstdio"
#include <ctype.h>
#include <fstream>

const int MAX_SIZE_STRING = 256;
const int DEFAULT_SIZE = 1;
#define TValue unsigned long 

struct TData {
    char *key;
    TValue keyValue;
};
void Assign(TData &first, TData &second);
void AddKey(TData &first, TData &second);
class TNode {
private:
    bool leaf;
    size_t countKeys;
    TData *data;
    TNode **cptrs;
    static size_t DEGREE;

    void InsertNonFull(TData &);
    int FindKey(char *);

    void DeleteFromLeaf(int);
    void DeleteFromNonLeaf(int, char *);

    TData& FindMax();
    TData& FindMin();

    void Fill(int);

    void BorrowFromPrev(int);
    void BorrowFromNext(int);

    void Split(int);
    void Merge(int);
public:
    //void Print();
    TNode(size_t);
    void Insert(TNode **,TData &);
    TData *Search(char *);

    void Delete(char *);
    void DeleteTree();

    bool SaveInFileNode(FILE *, TNode *);
    bool LoadInFileNode(FILE *, TNode *);

    ~TNode();
    friend class TBtree;
};

#endif //LAB_2_TNODE_H
