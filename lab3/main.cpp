
#include <cstdio>
#include "BTree.h"

#include <string.h>
#include <fstream>
#include <malloc.h>
const int SIZE_NODE = 200;
void LowerString(char *);
void Parsing(char &, char *, TValue &);


int main() {
    std::ofstream fout("benchTime", std::ios::app);
    TBtree tree(SIZE_NODE);
    char buffer[MAX_SIZE_STRING+DEFAULT_SIZE];
    char action;
    TValue value;
    TData temp;
    clock_t start = clock();
    while(true) {
        Parsing(action, buffer, value);
        if(action == 'E') {
            break;
        }
        switch(action) {
            case '+':
                temp.key = (char *)malloc(sizeof(char) * strlen(buffer) + 1);
                strcpy(temp.key, buffer);
                temp.keyValue = value;
                if(!tree.SearchWord(buffer)) {
                    tree.Insert(temp);
                } else {
                    printf("Exist\n");
                }
                free(temp.key);
                break;
            case '-':
                tree.DeleteNode(buffer);
                break;
            case 'S':
                switch (tree.SaveTree(buffer)) {
                    case 1:
                        printf("OK\n");
                        break;
                    case -1:
                        printf("ERROR: Couldn't create file\n");
                        break;
                    case 0:
                        printf("ERROR: Couldn't save btree in file\n");
                        break;
                }
                break;
            case 'L':
                switch (tree.LoadTree(buffer)) {
                    case 1:
                        printf("OK\n");
                        break;
                    case -1:
                        printf("ERROR: Couldn't open file\n");
                        break;
                    case 0:
                        printf("ERROR: Couldn't load btree in file\n");
                        break;
                }
                break;
            case 'F':
                TData *answer = tree.SearchWord(buffer);
                if (answer) {
                    
                    printf("OK: %lu\n", answer->keyValue);
                } else {
                    printf("NoSuchWord\n");
                }
                break;
        }
    }
    clock_t finish = clock();
    double time = (double) (finish - start)/ CLOCKS_PER_SEC;
    fout << time << " ";
    fout.close();
    return 0;
}

void LowerString(char *buffer) {
    int index = 0;
    while (buffer[index] != '\0') {
        if (buffer[index] >= 'A' && buffer[index] <= 'Z') {
            buffer[index] += -'A' + 'a';
        }
        index++;
    }
}

void Parsing(char &action, char *buffer, unsigned long &value) {
    char ch;
    size_t i = 0;
    ch = getchar();
    if (ch == EOF) {
        action = 'E';
        return;
    }
    if (ch == '+') {
        action = ch;
        getchar();
        scanf("%s %lu", buffer, &value);
        getchar();
        LowerString(buffer);
    } else if (ch == '-') {
        action = ch;
        getchar();
        scanf("%s", buffer);
        getchar();
        LowerString(buffer);
    } else if (ch == '!') {
        getchar();
        action = getchar();
        while((ch = getchar()) != ' ') {
            i++;
        }
        scanf("%s", buffer);
        getchar();
    } else {
        action = 'F';
        buffer[i++] = ch;
        while((ch = getchar()) != '\n') {
            buffer[i++] = ch;
        }
        buffer[i] = '\0';
        LowerString(buffer);
    }
}

