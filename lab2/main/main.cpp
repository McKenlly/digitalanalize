#include <cstdio>
#include "BTree.h"
#include <string.h>
//#include <fstream>

char LowerCase(char c);
bool IsLetter(char c);
void Parsing(char *action, char *buffer, unsigned long long *value);
void PrintItem(TData *temp);


int main() {
    //std::ofstream fout("benchmarkForTime", std::ios::app);
    TBtree tree(2);
    char buffer[MAX_SIZE_STRING+DEFAULT_SIZE];
    char action;
    TValue value;
    TData temp;
    //clock_t start = clock();
    while(true) {
        Parsing(&action, buffer, &value);
        if(action == 'E') {
            break;
        }
        switch(action) {
            case '+':
                temp.key = (char *)malloc(sizeof(char) * strlen(buffer) + 1);
                strcpy(temp.key, buffer);
                //temp.key = buffer;
                temp.keyValue = value;
                if(!tree.SearchWord(buffer)) {
                    tree.Insert(temp);
                } else {
                    printf("Exist\n");
                }
                free(temp.key);
                break;
            case '-':
                tree.DeleteNode(buffer);
                break;
            case 'S':
                if (tree.SaveTree(buffer) == 1) {
                    printf("OK\n");
                }
                else {
                    printf("ERROR: Couldn't create file\n");
                }
                break;
            case 'L':
                if (tree.LoadTree(buffer) == 1) {
                    printf("OK\n");
                }
                else {
                    printf("ERROR: Couldn't load file\n");
                }
                break;
            case 'F':
                PrintItem(tree.SearchWord(buffer));
                break;
        }
       // tree.Print();
    }
   // clock_t finish = clock();
    //double time = (double) (finish - start)/ CLOCKS_PER_SEC;
    //fout << time << std::endl;
    return 0;
}

char LowerCase(char c) {
    return c >= 'A' && c <= 'Z' ? c - 'A' + 'a' : c;
}

bool IsLetter(char c) {
    return c >= 'a' && c <= 'z' ? true : false;
}

void Parsing(char *action, char *buffer, unsigned long long *value) {
    char ch;
    size_t i = 0;
    bool flag = true;

    ch = getchar();

    if(ch == EOF) {
        *action = 'E';
        return;
    }

    if(ch == '+' || ch == '-') {
        *action = ch;
        getchar();
        while(flag) {
            ch = LowerCase(getchar());
            if(!IsLetter(ch)) {
                flag = false;
            } else {
                buffer[i++] = ch;
            }
        }
        buffer[i] = '\0';
        if(*action == '+') {
            *value = 0;

            while ((ch = getchar()) != '\n') {
                *value = (*value) * 10 + ch - '0';
            }
        }
    } else if(ch == '!') {
        getchar();
        while((ch = getchar()) != ' ') {
            buffer[i++] = ch;
        }
        buffer[i] = '\0';
        i = 0;
        *action = buffer[0];
        while((ch = getchar()) != '\n') {
            buffer[i++] = ch;
        }
        buffer[i] = '\0';
    } else {
        *action = 'F';
        buffer[0] = LowerCase(ch);
        i++;
        while((ch = getchar()) != '\n') {
            buffer[i++] = LowerCase(ch);
        }
        buffer[i] = '\0';
    }
}

void PrintItem(TData *temp) {
    if(temp) {
        printf("OK: %llu\n", temp->keyValue);
    } else {
        printf("NoSuchWord\n");
    }
}
