//
// Created by bokoch on 12.10.17.
//
#include "cstdio"
#ifndef LAB_2_BTREE_H
#define LAB_2_BTREE_H

#include "TNode.h"

const int DEFAULT_DEGREE = 2;
class TBtree {
private:
    TNode *root;
public:
    //void Print();
    explicit TBtree(size_t);
    void Insert(TData &);
    void DeleteNode(char *);
    TData *SearchWord(char *);
    int SaveTree(const char *);
    int LoadTree(const char *);
    ~TBtree();
    friend class TNode;
};

#endif //LAB_2_BTREE_H
