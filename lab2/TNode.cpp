#include <cstdlib>
#include <cstring>
#include "TNode.h"

size_t TNode::DEGREE = 2;
void Assign(TData &first, TData &second){
    char *temp = first.key;
    first.key = second.key;
    second.key = temp;
    first.keyValue = second.keyValue;
}
void AddKey(TData& first, TData& second) {
    first.key = (char *)malloc(sizeof(char) * strlen(second.key) + 1);
    strcpy(first.key, second.key);
    first.keyValue = second.keyValue;
}

TNode::TNode(size_t size) {
    countKeys = 0;
    leaf = true;
    data = (TData *)malloc(sizeof(TData) * (2 * size - 1));
    cptrs = (TNode **)malloc(sizeof(TNode *) * 2 * size);
    for(int i = 0; i < 2 * size; ++i) {
        cptrs[i] = nullptr;
        if(i < 2 * size - 1) {
            data[i].key = nullptr;
        }
    }
}

TData *TNode::Search(char *isKey) {
    size_t left = 0;
    size_t rigth = countKeys;
    while(left < rigth) {
        size_t mid = left + (rigth - left) / 2;
        if(strcmp(isKey, data[mid].key) > 0) {
            left = mid + 1;
        } else {
            rigth = mid;
        }
    }
    if(rigth < countKeys && strcmp(isKey, data[rigth].key) == 0){
        return &data[rigth];
    } else if (leaf) {
        return nullptr;
    } else {
        return cptrs[rigth]->Search(isKey);
    }
}

void TNode::Split(int index) {
    TNode *z = new TNode(DEGREE);
    TNode *y = cptrs[index];
    z->leaf = y->leaf;
    z->countKeys = DEGREE - 1;
    for(int j = 0; j < DEGREE - 1; ++j) {
        Assign(z->data[j], y->data[j+DEGREE]);
    }
    if(!y->leaf) {
        for(int j = 0; j < DEGREE; ++j) {
            z->cptrs[j] = y->cptrs[j + DEGREE];
        }
    }
    y->countKeys = DEGREE - 1;
    for(size_t j = countKeys; j >= index + 1; --j) {
        cptrs[j + 1] = cptrs[j];
    }
    cptrs[index + 1] = z;
    for(int j = countKeys - 1; j >= index; --j) {
        Assign(data[j+1], data[j]);
    }
    Assign(data[index], y->data[DEGREE-1]);
    ++countKeys;
}

void TNode::Merge(int index) {
    int i;
    TNode *left = cptrs[index];
    TNode *rigth = cptrs[index + 1];
    // копируем ключ в сына
    Assign(left->data[left->countKeys], data[index]);
    left->countKeys++;
    // Копируем ключи из правого сына в левого
    for(i = 0; i < rigth->countKeys; ++i) {
        Assign(left->data[i+left->countKeys], rigth->data[i]);
    }
    // копируем указатели, если не лист
    if(!left->leaf) {
        for(i = 0; i <= rigth->countKeys; ++i) {
            left->cptrs[i + left->countKeys] = rigth->cptrs[i];
        }
    }
    // сдвигаем в текущем узле после index ключи влево
    for(i = index + 1; i < countKeys; ++i) {
        Assign(data[i-1], data[i]);
    }
    // сдвигаем в текущем узле после index укахатели влево
    for(i = index + 2; i <= countKeys; ++i) {
        cptrs[i - 1] = cptrs[i];
    }
    cptrs[countKeys] = nullptr;
    left->countKeys += rigth->countKeys;
    --countKeys;
    delete rigth;
}

void TNode::Insert(TNode **root ,TData& temp) {
    if(countKeys == 2 * DEGREE - 1) {
        TNode *s = new TNode(DEGREE);
        *root = s;
        s->leaf = false;
        s->countKeys = 0;
        s->cptrs[0] = this;
        s->Split(0);
        s->InsertNonFull(temp);
    } else {
        (*root)->InsertNonFull(temp);
    }
}

void TNode::InsertNonFull(TData& tmp) {
    int i = countKeys - 1;
    if(leaf) {
        while(i >= 0 && 0 > strcmp(tmp.key, data[i].key)) {
            Assign(data[i+1], data[i]);
            --i;
        }
        AddKey(data[i+1], tmp);
        ++countKeys;
         printf("OK\n");
    } else {
        while(i >= 0 && 0 > strcmp(tmp.key, data[i].key)) {
            --i;   
        }
        ++i;
        if(cptrs[i]->countKeys == 2 * DEGREE - 1) {
            Split(i);
            if (0 < strcmp(tmp.key, data[i].key)) {
                ++i;
            }
        }
        cptrs[i]->InsertNonFull(tmp);
    }
}

int TNode::FindKey(char *isKey) {
    int left = 0;
    int rigth = countKeys;
    while(left < rigth) {
        int mid = left + (rigth - left) / 2;
        if(strcmp(isKey, data[mid].key) > 0) {
            left = mid + 1;
        } else {
            rigth = mid;
        }
    }
    return rigth;
}

void TNode::DeleteFromLeaf(int index) {
    for (int i = index + 1; i < countKeys; ++i) {
        Assign(data[i-1], data[i]);
    }
    --countKeys;
    free(data[countKeys].key);
    data[countKeys].key = nullptr;
    printf("OK\n");
}

TData &TNode::FindMax() {
    TNode *temp = this;
    while(!temp->leaf) {
        temp = temp->cptrs[temp->countKeys];
    }
    return temp->data[temp->countKeys - 1];
}

TData &TNode::FindMin() {
    TNode *temp = this;
    while(!temp->leaf) {
        temp = temp->cptrs[0];
    }
    return temp->data[0];
}

void TNode::Fill(int index) {
    if(index && cptrs[index - 1]->countKeys >= DEGREE) {
        BorrowFromPrev(index);
    } else if (index!= countKeys && cptrs[index + 1 ]->countKeys >= DEGREE) {
        BorrowFromNext(index);
    } else {
        if (index != countKeys) {
            Merge(index);
        }
        else
            Merge(index - 1);
    }
}

void TNode::BorrowFromPrev(int index) {
    TNode *child = cptrs[index];
    TNode *sibling = cptrs[index - 1];
    for (int i = child->countKeys - 1; i >= 0; --i) {
        Assign(child->data[i + 1], child->data[i]);
    }
    if (!child->leaf) {
        for (int i = child->countKeys; i>=0; --i)
            child->cptrs[i + 1] = child->cptrs[i];
    }
    Assign(child->data[0], data[index - 1]);
    if (!leaf) {
        child->cptrs[0] = sibling->cptrs[sibling->countKeys];
    }
    Assign(data[index - 1], sibling->data[sibling->countKeys - 1]);
    ++child->countKeys;
    --sibling->countKeys;
}

void TNode::BorrowFromNext(int index) {
    TNode *child=cptrs[index];
    TNode *sibling=cptrs[index+ 1];
    Assign(child->data[(child->countKeys)], data[index]);
    if (!(child->leaf)) {
        child->cptrs[(child->countKeys)+1] = sibling->cptrs[0];
    }
    Assign(data[index], sibling->data[0]);
    for (int i = 1; i<sibling->countKeys; ++i) {
        Assign(sibling->data[i-1], sibling->data[i]);
    }
    if (!sibling->leaf) {
        for(int i = 1; i <= sibling->countKeys; ++i)
            sibling->cptrs[i - 1] = sibling->cptrs[i];
    }
    ++child->countKeys;
    --sibling->countKeys;
}

void TNode::DeleteFromNonLeaf(int index, char* isKey) {
    if(cptrs[index]->countKeys >= DEGREE) {
        TData &temp = cptrs[index]->FindMax();
        free(data[index].key);
        AddKey(data[index], temp);
        cptrs[index]->Delete(temp.key);
    } else if(cptrs[index + 1]->countKeys >= DEGREE) {
        TData &temp = cptrs[index + 1]->FindMin();
        free(data[index].key);
        AddKey(data[index], temp);
        cptrs[index + 1]->Delete(temp.key);
    } else {
        Merge(index);
        cptrs[index]->Delete(isKey);
    }
}

void TNode::Delete(char *temp) {
    int index = FindKey(temp);
    if (index < countKeys && strcmp(data[index].key, temp) == 0) {
        if(leaf) {
            DeleteFromLeaf(index);
        } else {
            DeleteFromNonLeaf(index, temp);
        } 
    } else {
        if (leaf) {
            printf("NoSuchWord\n");
            return;
        }
        if (cptrs[index]->countKeys < DEGREE) {
            Fill(index);
        }
        if (index > countKeys) {
            cptrs[index - 1]->Delete(temp);
        } else {
            cptrs[index]->Delete(temp);
        }
    }
}

bool TNode::SaveInFileNode(FILE* fileSave, TNode *root) {
    if (fwrite(&root->countKeys, sizeof(root->countKeys), 1, fileSave) != 1) {
        return false;
    }

    if (fwrite(&root->leaf, sizeof(root->leaf), 1, fileSave) != 1) {
        return false;
    }
    for (size_t i = 0; i < root->countKeys; ++i) {
        TData* dataT = &root->data[i];
        const size_t strLen = strlen(dataT->key);
        const char* str = dataT->key;
        if (fwrite(&strLen, sizeof(strLen), 1, fileSave) != 1) {
            return false;
        }
        if (fwrite(str, sizeof(char), strLen, fileSave) != strLen) {
            return false;
        }
        if (fwrite(&dataT->keyValue, sizeof(dataT->keyValue), 1, fileSave) != 1) {
            return false;
        }
    }
    if (!root->leaf) {
        for (size_t i = 0; i < root->countKeys + 1; ++i) {
            if (!SaveInFileNode(fileSave, root->cptrs[i])) {
                return false;
            }
        }
    }
    return true;
}

bool TNode::LoadInFileNode(FILE* fileLoad, TNode *root) {
    char buffer[MAX_SIZE_STRING+DEFAULT_SIZE];

    if (fread(&root->countKeys, sizeof(root->countKeys), 1, fileLoad) != 1) {
        return false;
    }

    if (fread(&root->leaf, sizeof(root->leaf), 1, fileLoad) != 1) {
        return false;
    }

    for (size_t i = 0; i < root->countKeys; ++i) {
        TData* dataT = &root->data[i];
        size_t strLen;
        if (fread(&strLen, sizeof(strLen), 1, fileLoad) != 1) {
            return false;
        }
        if (fread(buffer, sizeof(char), strLen, fileLoad) != strLen) {
            return false;
        }
        if (fread(&dataT->keyValue, sizeof(dataT->keyValue), 1, fileLoad) != 1) {
            return false;
        }
        buffer[strLen] = '\0';
        dataT->key = (char *)malloc(sizeof(char) * (strLen+DEFAULT_SIZE));
        strcpy(dataT->key, buffer);
    }
    if (!root->leaf) {
        for (size_t i = 0; i < root->countKeys + 1; ++i) {
            root->cptrs[i] = new TNode(DEGREE);
            if (!LoadInFileNode(fileLoad, root->cptrs[i])) {
                return false;
            }
        }
    }
    return true;
}

TNode::~TNode() {
    for(int i = 0; i < countKeys; ++i) {
        free(data[i].key);
    }
    free(data);
    free(cptrs);
}

void TNode::DeleteTree() {
    TNode *tmp = this;
    if (tmp == nullptr) {
        return;
    }
    if (!leaf) {
        for (int i = 0; i < tmp->countKeys + 1; ++i) {
            tmp->cptrs[i]->DeleteTree();
        }
    }
    delete(tmp);
}
