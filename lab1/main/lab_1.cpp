//Project Radix_sort
#include "TData.h"
#include "TVector.h"
int main() {
    TVector in(32);
    TData t;
    while (t.InputStream()) {
        in.Push_back(t);
    }
    if(in.Size() == 1){
        in[0].Print();
        return 0;
    }
    if(in.Size() > 0) {
        in.RadixSort();
    }
    return 0;
}

