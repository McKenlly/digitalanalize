import random
import string

MAX_VALUE_LEN = 10

SIZE_ARRAY = 10000000
def generate_random_value():
    return "".join([random.choice(string.ascii_lowercase) for _ in range(1, MAX_VALUE_LEN)])


if __name__ == "__main__":
    for num in range(1, 2):
        values = list()
        output_filename = "tests/{:02d}.t".format(num)
        with open(output_filename, 'w') as output:
            for _ in range(0, SIZE_ARRAY+1):
                keyString = "+"+str(random.randint(0, 999))+"-"+str(random.randint(100, 999))+"-"+str(random.randint(1000000, 9999999))
                value = generate_random_value()
                #tmp = keyString
                #tmp = tmp[1:]
                #tmp = tmp.split('-')
                #key = int(tmp[0]+tmp[1]+tmp[2]);
                tmp = keyString.replace("+", "")
                tmp = tmp.replace("-", "")
                key = int(tmp)
                values.append((key, keyString, value))
                output.write("{}\t{}\n".format(keyString, value))
        # Answer.
        # values[0][0] -- key
        # values[0][1] -- value
        # values[0][2] -- keyString
        output_filename = "tests/{:02d}.txt".format(num)
        with open(output_filename, 'w') as output:
            values = sorted(values, key=lambda x: x[0])
            for value in values:
                #print(value[0])
                output.write("{}\t{}\n".format(value[1], value[2]))
