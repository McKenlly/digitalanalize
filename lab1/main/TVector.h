#ifndef TVECTOR_H
#define TVECTOR_H

#include "TData.h"
#include "cstring"
#include <cmath>
const int BYTES_BITS = 8;
const int BITS_IN_PHONE = 45;
class TVector {
private:
    size_t size;
    size_t capacity;
    TData *array;
public:
    TVector();

    TVector(const size_t &);

    TData &operator[](const size_t &);

    size_t Size() const;

    size_t Capacity() const;

    void Push_back(const TData &);

    friend TKey GetBits(const TKey &, const size_t&, const int&, const TKey&);
    void CountingSort(const TKey &, const int&, const size_t &, int *);

    void RadixSort(/*TVector&*/);
    friend int OptimalRank(int, int);
    friend int RoundDown(double);
    friend TKey GetBits(const TKey&, const size_t&,
                        const int&, const TKey&);
    ~TVector();
};


#endif //LAB1_TVECTOR_H
