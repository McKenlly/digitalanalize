#include <cstdlib>
#include "TVector.h"
TVector::TVector() {
    array = nullptr;
    size = 0;
    capacity = 0;
}

TVector::TVector(const size_t & sizeVector) {
    capacity = sizeVector;
    size = 0;
    array = new TData[sizeVector];
}
void TVector::Push_back(const TData &temp) {
    if (size == capacity) {
        capacity *= 2;
        TData *result = new TData[capacity];
        for (int index = 0; index < size; index++) {
            result[index] = array[index];
        }
        delete[] array;
        this->array = result;
    }
    array[size++] = temp;
}


size_t TVector::Size() const {
    return size;
}

size_t TVector::Capacity() const {
    return capacity;
}

TData& TVector::operator[](const size_t & index) {
    return array[index];
}

TKey GetBits(const TKey& number, const size_t& shift,
                                    const int& bits, const TKey& maxDigit) {
    return (number >> shift * bits)&(maxDigit-1);
}


void TVector::CountingSort(const TKey & maxDigit, const int & rank,
                           const size_t & shift, int *indexArr) {
    int *count = new int[maxDigit];
    for (int i = 0; i < maxDigit; i++) {
        count[i] = 0;
    }

    for (int i = 0; i < size; i++) {
        TKey temp = GetBits(array[indexArr[i]].GetKey(), shift, rank, maxDigit);
        count[temp]++;
    }

    for (int i = 1; i < maxDigit; i++) {
        count[i] += count[i-1];
    }
    int *result = new int[size];
    for (int i = (int)size - 1; i >= 0; i--) {
        TKey temp = GetBits(array[indexArr[i]].GetKey(), shift, rank, maxDigit);
        result[count[temp] - 1] = indexArr[i];
        count[temp]--;
    }

    for (int i = 0; i < size; i++) {
       indexArr[i] = result[i];
    }
    //free memory
    delete [] count;
    delete [] result;
}

int RoundDown (double t) {
    double val;
    modf(t, &val);
    return (int)val;
}

int OptimalRank (int n, int b) {
    if (b < n) {
        return b;
    }
    return n;
}

void TVector::RadixSort(/*TVector& answer*/) {
    int *indexArray = new int[size];
    for (int i = 0; i < size; i++) indexArray[i] = i;
    int bitsWord = sizeof(TKey) * BYTES_BITS;
    //maxDigits - максимальное число в r битах - оптимальный промежуток 0..2^r-1
    int rank = OptimalRank(RoundDown(log2(size)), bitsWord);
    TKey maxDigits = 1 << rank; // R

    for (int shift = 0; shift < ceil(BITS_IN_PHONE*1.0/rank); shift++) {
        CountingSort(maxDigits, rank, shift, indexArray);
    }
    for (int i = 0; i < size; i++) {
        array[indexArray[i]].Print();
    }
    delete [] indexArray;
}

TVector::~TVector() {
    delete [] array;
}
