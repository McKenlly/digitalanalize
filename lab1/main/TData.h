#ifndef TData_H
#define TData_H
#include "TString.h"
#include <cstdint>
#include "cstdio"

int const MAX_SIZE_VALUE = 2048;
int const MAX_SIZE_KEY = 20;
using TKey = unsigned long;

class TData {
private:
    TKey key;
    TString keyString;
    TString value;
public:
    TData();
    TData(const TData&);
    TData &operator=(const TData &obj);

    void Print();

    TKey GetKey();
    char* GetNumber();
    char* GetValue();
    int InputStream();

    ~TData();
};

#endif
