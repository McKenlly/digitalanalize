#include <cstring>
#include "TData.h"
#include <iostream>

TData::TData() {
    key = 0;
}
TData::TData(const TData &orig) {
    value = orig.value;
    keyString = orig.keyString;
    key = orig.key;
}

TData &TData::operator=(const TData &obj) {
    if (this == &obj)
        return *this;
    key = obj.key;
    keyString = obj.keyString;
    value = obj.value;
    return *this;
}

void TData::Print() {
    printf("%s\t%s\n", keyString.GetSting(), value.GetSting());
}

int TData::InputStream() {
    char *temp = new char[MAX_SIZE_KEY];
    if (scanf("%s", temp) == EOF) {
        delete [] temp;
        return 0;
    }
    key = 0;
    keyString = temp;
    int sizeDig = keyString.Size();
    TKey degree = 1;
    for (int i = sizeDig - 1; i >= 1; i--) {
        if (keyString[i] != '-') {
            key += (int) (keyString[i] - '0') * degree;
            degree *= 10;
        }
    }
    //input value
    delete [] temp;
    temp = new char[MAX_SIZE_VALUE];
    scanf("%s", temp);
    value = temp;
    delete [] temp;
    return 1;
}

TKey TData::GetKey(){
    return key;
}
char* TData::GetNumber(){
    return keyString.GetSting();
}
char* TData::GetValue() {
    return value.GetSting();
}
TData::~TData() {
}
