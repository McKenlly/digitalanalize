import random
import string

MAX_VALUE_LEN = 2048

SIZE_ARRAY = 2
def generate_random_value():
    return "".join([random.choice(string.ascii_lowercase) for _ in range(1, MAX_VALUE_LEN)])


if __name__ == "__main__":
    for num in range(1, 17):
        values = list()
        output_filename = "tests/{:02d}.t".format(num)
        with open(output_filename, 'w') as output:
            for _ in range(0, SIZE_ARRAY+1):
                key = random.randint(10000000000000, 99999999900000);
                value = generate_random_value()
                values.append((key, value))
                output.write("{}\t{}\n".format(key, value))
        SIZE_ARRAY = SIZE_ARRAY * 2
