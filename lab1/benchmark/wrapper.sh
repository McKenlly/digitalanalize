#if ! make; then
#    echo "ERROR: Failed to compile file."
#    exit 1
#fi

if ! python3 generatorBenchmark.py ; then
echo "ERROR: Failed to python generate tests."
exit 1
fi

for test_file in `ls tests/*.t`; do
    echo "Execute ${test_file}"
    if ! ./benchmark < $test_file ; then
        echo "ERROR"
        continue
    fi
    answer_file="${test_file%.*}"

    if ! diff -u "${answer_file}.txt" tmp > /dev/null ; then
        echo "Failed"
    else
        echo "OK"
    fi 
done 
