#include <iostream>
#include <vector>
#include <algorithm>
#include "cmath"
#include <ctime>
const int BITS_IN_PHONE = 45;
const int BYTES_BITS = 8;

using Ttype = unsigned long;
using Tpair = std::pair <Ttype, std::string>;

bool PairCompair(const Tpair &a,
                 const Tpair &b);

void RunStdSort(std::vector<Tpair > &data);

void RunRadixSort(std::vector<Tpair > &data);

void RadixSort(std::vector<Tpair > &data, std::vector<Tpair > &result);

Ttype GetBits(Ttype number, const size_t shift,
              const int bits, const Ttype&);

int OptimalRank(int n, int b);

int RoundDown(double t);

void CountingSort(const Ttype &k, const int &rank,
                  const size_t &shift,
                  std::vector<Tpair > &data,
                  int *indexArray);
void RadixSort(std::vector<Tpair > &data);

int main() {
    std::vector<Tpair > forSortStd;
    std::vector<Tpair > forSortRadix;

    Ttype key;
    std::string value;
    Tpair temp;
    while (std::cin >> key >> value) {
        temp.first = key;
        temp.second = value;
        forSortStd.push_back(temp);
        forSortRadix.push_back(temp);
    }
    std::cout << "Size = " << forSortRadix.size() << std::endl; 
    RunStdSort(forSortStd);
    RunRadixSort(forSortRadix);
}

bool PairCompair(const Tpair &a,
                 const Tpair &b) {
    return a.first < b.first;
}

void RunStdSort(std::vector<Tpair > &data) {
    clock_t start = clock();
    std::sort(data.begin(), data.end(), PairCompair);
    clock_t end = clock();
    double time = (double) (end - start);
    printf("Std   SortTime = %lf\n", time / CLOCKS_PER_SEC);
}

void RunRadixSort(std::vector<Tpair > &data) {
    std::vector<Tpair > result(data.size());
    clock_t start = clock();
    RadixSort(data, result);
    clock_t end = clock();
    double time = (double) (end - start);
    printf("Radix SortTime = %lf\n", time / CLOCKS_PER_SEC);
}

void CountingSort(const Ttype &k, const int &rank, const size_t &shift,
                  std::vector<Tpair > &data,
                  int *indexArray) {
    int *count = new int[k];
    for (int i = 0; i < k; i++) {
        count[i] = 0;
    }
    int *result = new int [data.size()];
    for (int i = 0; i < data.size(); i++) {
        Ttype temp = GetBits(data[indexArray[i]].first, shift, rank, k);
        count[temp]++;
    }

    for (int i = 1; i < k; i++) {
        count[i] += count[i - 1];
    }
    for (int i = data.size() - 1; i >= 0; i--) {
        Ttype temp = GetBits(data[indexArray[i]].first, shift, rank, k);
        result[--count[temp]] = indexArray[i];
    }

    for (int i = 0; i < data.size(); i++) {
        indexArray[i] = result[i];
    }
    //free memory
    delete[] count;
    delete[] result;
}
Ttype GetBits(Ttype number, const size_t shift,
             const int bits, const Ttype& M) {
    return (number >> shift * bits)&(M-1);
}
int RoundDown(double t) {
    double val;
    modf(t, &val);
    return (int) val;
}

int OptimalRank(int n, int b) {
    if (b < n) {
        return b;
    }
    return n;
}

void RadixSort(std::vector<Tpair > &data,
               std::vector<Tpair > &result) {
    int bitsWord = sizeof(Ttype) * BYTES_BITS;
    int *indexArray = new int[data.size()];
    for (int i = 0; i < data.size(); i++) indexArray[i] = i;
    //maxDigits - максимальное число в r битах - оптимальный промежуток 0..2^r-1
    int rank = OptimalRank(RoundDown(log2(data.size())), bitsWord);
    Ttype maxDigits = 1 << rank; // R
    for (int shift = 0; shift < ceil(BITS_IN_PHONE/rank); shift++) {
        CountingSort(maxDigits, rank, shift, data, indexArray);
    }
    for (int i = 0; i < data.size(); i++) {
        result[i] = data[indexArray[i]];
    }
    delete [] indexArray;
}
