#include <iostream>
#include <algorithm>
#include <vector>
#include "TBigInt.h"
#include <ctime>
#include <cstdio>

int main(int argc, char *argv[]) {
	
	std::string lineNum1, lineNum2;
	char sign;

	FILE* fout_multiply;
	FILE* fout_division;
	FILE* fout_addition;
	FILE* fout_subtraction;

	fout_multiply = fopen("benchmark/time/multiply_main.txt", "a+");
	fout_division = fopen("benchmark/time/division_main.txt", "a+");
	fout_addition = fopen("benchmark/time/addition_main.txt", "a+");
	fout_subtraction = fopen("benchmark/time/subtraction_main.txt", "a+");

	clock_t start;
	clock_t finish;
	
	double time;

	while (std::cin >> lineNum1 >> lineNum2 >> sign) {
		TBigInt a(lineNum1);
		TBigInt b(lineNum2);
		TBigInt result;
		switch (sign) 
		{
			case '+': {
					start = clock();
				    result = a + b;
				    finish = clock();
				    time = (double) (finish - start)/CLOCKS_PER_SEC;
				    fprintf(fout_addition, "%0.15f\n", time);
					std::cout << result << std::endl;
				}
				break;
			case '-':
				if (a < b) {
					std::cout<< "Error\n";
				} else {
					start = clock();
					result = a - b;
					finish = clock();
				    time = (double) (finish - start)/CLOCKS_PER_SEC;
				    fprintf(fout_subtraction, "%0.15f\n", time);
					std::cout << result << std::endl;
					break;
				}
                break;
			case '/':
                if (b == 0) {
                    std::cout << "Error\n";
                } else {
                    start = clock();
             
           			result = a / b;
                
           			finish = clock();             			
           			time = (double) (finish - start)/CLOCKS_PER_SEC;
					fprintf(fout_division, "%0.15f\n", time);
					std::cout << result << std::endl;
                }
				break;
			case '*':
                start = clock();
                
                result = a * b;
                
                finish = clock();
                time = (double) (finish - start)/CLOCKS_PER_SEC;
				fprintf(fout_multiply, "%0.15f\n", time);
				std::cout << result << std::endl;
				break;
			case '^':
                result = a ^ b;
				std::cout << result << std::endl;
				break;
			case '>':
				std::cout << (a > b ? "true" : "false") << std::endl;
				break;
			case '<':
				std::cout << (a < b ? "true" : "false") << std::endl;
				break;
			case '=':
				std::cout << (a == b ? "true" : "false") << std::endl;
				break;
			default:
				std::cout << "Uncorrect requests" << std::endl;
				break;
		}
	}
	
	fclose(fout_addition);
	fclose(fout_subtraction);
	fclose(fout_multiply);
	fclose(fout_division);
	return 0;
}
