import matplotlib.pyplot as plt
import numpy as np

count_request = 18
max_size = 262144
PATH_TIME = "benchmark/time/"

def load_time(operation):
	with open( PATH_TIME + operation + "_main.txt", 'r' ) as time_main, \
			open( PATH_TIME +operation + "_ntl.txt", "r" ) as time_ntl:
			main = np.array([ float(i) for i in time_main.read().split() ])
			ntl =  np.array([ float(i) for i in time_ntl.read().split() ])
	return main, ntl
	


if __name__ == '__main__':
	operations = ["addition"]
	for i in operations:
		main_y, ntl_y = load_time(i)
		X = np.linspace(2, 262144, num = len(main_y))
		print(len(main_y), len(ntl_y), len(X))
		line_main, line_ntl = plt.plot(X, main_y, 'red', X, ntl_y, 'blue')
		#plt.axis([0, max_size, 0, 0.5])
		plt.title(u'Зависимость количества разрядов в числе от времени')
		plt.xlabel(u'Количество разрядов')
		plt.ylabel(u'Время')
		plt.legend( (line_main, line_ntl), 
			(u'include my program', u'include NTL'), loc="best")
		plt.savefig(i + '.png', format = 'png')
