import sys
import random
import string
import copy
from random import choice
count_of_tests = 4
num_requests_in_test = 18

num = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0']
def generate_num(count_rows_in_num):
	return ''.join(choice(num) for i in range(count_rows_in_num))


if __name__=="__main__":
	actions = ["+", "-", "/", "*"]
	condition = [">", "<", "="]
	for enum in range( count_of_tests ):
		step = 2
		count_rows_in_num = 1
		test_file_name = "tests/{:02d}".format( enum + 1 )
		with open( "{0}.t".format( test_file_name ), 'w' ) as output_file, \
			open( "{0}.txt".format( test_file_name ), "w" ) as answer_file:
			for i in range(num_requests_in_test): 
				count_rows_in_num *= step 
				a = generate_num(count_rows_in_num)
				b = generate_num(count_rows_in_num)
				sign = random.choice(actions[enum])
				output_file.write("{0}\n{1}\n{2}\n".format( a, b, sign ))
				a = int(a)
				b = int(b)
				answer = ""
				if (sign == '+'):
					answer = str(a+b)
				elif sign == '-':
					answer = (str(a-b), "Error")[a < b]
				elif sign == '*':
					answer = str(a*b)
				elif sign == '/':
					answer = str(a//b)
				elif sign == '^':
					answer = str(a**b)
				elif sign == '<':
					answer = ("false", "true")[a < b]
				elif sign == '>':
					answer = ("false", "true")[a > b]
				elif sign == '=':
					answer = ("false", "true")[a == b]
				answer_file.write("{0}\n".format( answer ))




