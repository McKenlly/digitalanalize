#!/bin/bash
#clear old tests
rm -rf tests/

mkdir tests
#compile cheker
echo "Compile cheker"
python3 checker.py

#genering program
rm lab6 *.o
make all

echo "Execute tests"
for test_file in `ls tests/*.t`; do
 echo "Execute ${test_file}"
    if ! ./lab6 < $test_file > tmp ; then
        echo "ERROR"
        continue
    fi
    answer_file="${test_file%.*}"

    if ! diff -u "${answer_file}.txt" tmp > /dev/null ; then
        echo "Failed"
    else
        echo "OK"
    fi 
done 
echo "Finish"
rm tmp

