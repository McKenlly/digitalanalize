//
// Created by mckenlly on 17.05.18.
//

#ifndef LAB9_TGRAPH_H
#define LAB9_TGRAPH_H


#include <vector>
#include <istream>
#include <set>

class TGraph {
private:
    std::vector<std::set<std::size_t> > data;
    std::vector<std::set<std::size_t> > components;
    std::size_t size;
public:
    explicit TGraph();

    std::vector<std::set<std::size_t> > FindComponents();
    void DFS(std::size_t start, std::vector<bool>& used_vertex);
    friend std::istream &operator>>(std::istream &is, TGraph &graph);
    ~TGraph() {}
};


#endif //LAB9_TGRAPH_H
