#include <iostream>
#include "TGraph.h"

int main() {
    TGraph graph;
    std::cin >> graph;
    std::vector<std::set<std::size_t> > answer = graph.FindComponents();

    for (int i = 0; i < answer.size(); i++) {
        std::set<std::size_t>::iterator it = answer[i].end();
        it--;
        for (std::set<std::size_t>::iterator j = answer[i].begin(); j != it; j++) {
            std::cout << *j+1 << " ";
        }
        
        std::cout <<*it+1 <<std::endl;
    }
    return 0;
}