import sys
import random
import string
import copy
from random import choice
count_of_tests = 4
num_requests_in_test = 18

class Data(object):
    def __init__(self, name):
        self.__name  = name
        self.__links = set()
    
    @property
    def name(self):
        return self.__name
    
    @property
    def links(self):
        return set(self.__links)
    
    def add_link(self, other):
        self.__links.add(other)
        other.__links.add(self)
    
def connected_components(nodes):
    result = []
    nodes = set(nodes)
    while nodes:
        n = nodes.pop()
        group = {n}
        queue = [n]
        while queue:
            n = queue.pop(0)
            neighbors = n.links
            neighbors.difference_update(group)
            nodes.difference_update(neighbors)
            group.update(neighbors)
            queue.extend(neighbors)
    
        result.append(group)
    
    return result


def generate_num(count_vertex):
    return random.randint(1, count_vertex+1)


if __name__=="__main__":
    
    for enum in range( count_of_tests ):
        step = 2
        test_file_name = "tests/{:02d}".format( enum + 1 )
        with open( "{0}.t".format( test_file_name ), 'w' ) as output_file, \
            open( "{0}.txt".format( test_file_name ), "w" ) as answer_file:
            graph = {}
            n = generate_num(100)
            m = generate_num(n)
            output_file.write("{0}\t{1}\n", n, m)
            for i in range(m): 
                a = generate_num(count_rows_in_num)
                b = generate_num(count_rows_in_num)
                
                #answer_file.write("{0}\n".format( answer ))



