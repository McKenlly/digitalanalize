//

#include "TGraph.h"

//
// Created by mckenlly on 17.05.18.





void TGraph::DFS(std::size_t start, std::vector<bool>& used_vertex) {
    used_vertex[start] = true;
    components[components.size() - 1].insert(start);
    for (std::set<std::size_t>::iterator it = data[start].begin();
                                    it != data[start].end(); ++it) {

        if (used_vertex[*it] == false)
            DFS(*it, used_vertex);
    }
}


std::vector<std::set<std::size_t> >  TGraph::FindComponents() {
    std::size_t size = data.size();
    std::vector<bool> used_vertex(size, false);

    for (std::size_t i = 0; i < size; ++i)
        if (used_vertex[i] == false) {
            components.emplace_back();
            DFS(i, used_vertex);

        }
    return components;
}

TGraph::TGraph() = default;

std::istream &operator>>(std::istream &is, TGraph &graph){
    std::size_t n, m;
    is >> n >> m;
    graph.data.resize(n);
    for (int i = 0; i < m; i++) {
        std::size_t x, y;
        is >> x >> y;
        graph.data[x-1].insert(y-1);
        graph.data[y-1].insert(x-1);
    }
    return is;
}
